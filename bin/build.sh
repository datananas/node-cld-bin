#!/usr/bin/env sh

set -e

# if we have a binding.gyp file, npm will build at install
cp binding-gyp binding.gyp

echo "Start build"
node-gyp rebuild # rebuild is an alias for clean, configure and build
npm run postbuild

echo "Clean build"
rm -r build/Release/obj.target build/Release/.deps
rm binding.gyp

echo "Run tests"
npm test

if command -v md5sum >/dev/null 2>&1; then
  echo "MD5 checksum"
  md5sum build/Release/cld.node
fi
