# THIS REPOSITORY IS DEPRECATED [YOU SHOULD BE THERE](https://github.com/Datananas/node-cld-bin)

Datananas node-cld-bin
======================


Caution
-------

This project is just a binary packages provider of the original `node-cld` project.
Modifications are listed in the `NOTICE`.

For more informations, the [original repository](https://github.com/dachev/node-cld) is a good place.


How to build
------------

Build dependencies are the same as the upstream module.

If you want to manually build the module, just run `npm run build`.

Look at the `bin/build.sh` script to see how it works, but that's not a big deal.
